import argparse
import os.path
import subprocess
import json


# start with an arbitrary list of codecs

codecs = ["libx264"]  # add codecs to the list : "libaom-av1", "libvpx-vp9", "libx265"
ffmpeg_bin = "ffmpeg"  # in case of different version name
time_bin = "time"  # still have not managed to use this


class Media(object):
    def __init__(self, filename):
        self.name = filename
        self.prefix, self.extension = os.path.splitext(filename)
        self.data = {  # dictionary that will output to json
            "filename": filename,
            "encoded_files": [
            ]
        }

    def encode(self, codec):

        _fullname = self.prefix + '_' + codec + '_ultrafast' + self.extension  # name depends on original media file
        _compressed = Media(_fullname)  # creating a new object along with dict

        _encoding_args = ["-hide_banner", "-i", self.name, "-c:v", codec, "-preset", "ultrafast", _compressed.name,
                          '-benchmark']
        # for now the encoding runs on ultrafast until we implement our own presets in the presets/ section

        subprocess.Popen([ffmpeg_bin] + _encoding_args)  # running command

        return _compressed

    def quality(self, compressed):

        _quality_args = ["-hide_banner", "-i", compressed.name, "-i", self.name, "-lavfi"]
        _psnr_arg = ["libvmaf=psnr=true:log_path=psnrlog.json:log_fmt=json", "-f", "null", "-"]
        _ssim_arg = ["libvmaf=ssim=true:log_path=ssimlog.json:log_fmt=json", "-f", "null", "-"]
        _vmaf_arg = ["libvmaf=log_path=vmaflog.json:log_fmt=json", "-f", "null", "-"]

        # PSNR
        subprocess.Popen([ffmpeg_bin] + _quality_args + _psnr_arg)
        # SSIM
        subprocess.Popen([ffmpeg_bin] + _quality_args + _ssim_arg)
        # VMAF
        subprocess.Popen([ffmpeg_bin] + _quality_args + _vmaf_arg)

    def tojson(self, num):
        # Dictionary to be added to self data to be merged into self.data[]
        _encode_dict = {
            "encoder": codec,
            "time": "",
            "psnr": "",
            "ssim": "",
            "vmaf": ""
        }

        # add the encoding dict above to the file dict
        self.data["encoded_files"].append(_encode_dict)

        # measure names along their reference in the import data file
        _measures = {
            "psnr": "psnr_y",
            "ssim": "float_ssim",
            "vmaf": "vmaf"
        }

        for _keyM, _valueM in _measures.items():

            _file = open(_keyM + "log.json", 'r')  # open import data
            _data = json.load(_file)

            for _keyE, _valueE in _data.items():
                if _keyE == 'pooled_metrics':
                    self.data['encoded_files'][num][_keyM] = _valueE[_valueM]['harmonic_mean']  # fetch info
            _file.close()

            with open("data.json", "w") as outfile:
                json.dump(self.data, outfile)  # write to .json output


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="A simple ffmpeg encoding comparator")
    parser.add_argument("sample", type=str, help="file to run the encoding comparison on")
    args = parser.parse_args()
    # Create a media object
    sample = Media(args.sample)

    for codec in codecs:  # encode the sample with each of the codecs in the list
        encoded_file = sample.encode(codec)  # encode file

        sample.quality(encoded_file)  # measure quality

        sample.tojson(codecs.index(codec))  # output results to json
