# Encoding Comparison

Encoding time and quality comparison for video codecs: x264, x265, vp9, aom-av1
Based on ffmpeg, psnr, ssim, vmaf

#### How to use:
* input media
* output: encoded medias + quality measures.json

run main.py <sample>
the script encodes the sample in all available codecs, then measure the psnr, ssim and vmaf evaluation of each encoded file and outputs it into .json files



#### TODO as of 17th of June 2022
* make own presets available in presets/
* setup a .json interpreter to export data in a graph of some kind
* structure json export
* measure time

### What to put in the json?
* 1 json per file
* time
* encoder {vmaf psnr ssim}


